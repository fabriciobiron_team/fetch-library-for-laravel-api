import config from './config.json'

class ApiCredential{

    constructor()
    {
        this.oauth = {
            authorization_route: config.oauth.authorization_route,
            grant_type: config.oauth.grant_type,
            scope: config.oauth.scope,
            client_id: config.oauth.client_id,
            client_secret: config.oauth.client_secret,
            credentials: {}
        }
    }

    set(username,password)
    {
        this.oauth.credentials = {
            "grant_type": this.oauth.grant_type,
            "client_id": this.oauth.client_id,
            "client_secret": this.oauth.client_secret,
            "username": username,
            "password": password,
            "scope": this.oauth.scope
        }
        return this.oauth
    }

    get()
    {
        return this.oauth.credentials
    }

}

export default new ApiCredential