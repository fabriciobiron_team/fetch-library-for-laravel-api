

import config from './config.json'
import ApiCredential from './ApiCredential';

/**
 * Handles Requests and OAuth for Laravel
 * @author Fabricio Biron <fabriciobiron@gmail.com>
 * @see http://fabriciobiron.me
 * @version 0.1.1
 * 
 */
class HttpService{

    constructor()
    {
        // console.log(config)
        this.api_url = config.app_url;
        this.api_prefix = config.api_prefix
        this.oauth = {}
        this.headers = {}
    }

    getApiURL()
    {
        return this.api_url
    }

    logout(){
        sessionStorage.removeItem('requestServiceHeaders')
        sessionStorage.removeItem('oauthAuthorization')
    }

    test(){
        return 'guenta'
    }
    /**
     * Sets OAuthcredentials 
     * @uses ApiCredential class
     * @param {string} username 
     * @param {string} password 
     */
    setCredentials(username,password)
    {
        ApiCredential.set(username,password)
    }


    /**
     * Gets OAuth Credentials
     *  @returns Object
     */
    getCredentials()
    {
        return ApiCredential.get()
    }
    /**
     * Gets header
     */
    getHeader()
    {
        // return this.headers     
        let parsed = JSON.parse(sessionStorage.getItem("requestServiceHeaders"))   
        // $parsed = JSON.parse(localStorage.requestServiceHeaders)   
        return parsed        
    }

    /**
     * Sets headers
     * @param {string} authorization 
     */
    setHeader(authorization)
    {
        this.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${authorization}`,
        }
        // console.log(this.headers)

        sessionStorage.setItem(
            'requestServiceHeaders' ,
            JSON.stringify({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${authorization}`,
            })
        )
    }

    setToken(authorization)
    {
        sessionStorage.setItem(
            'oauthAuthorization' ,
            authorization
        )
    }

    getToken()
    {
        return sessionStorage.getItem("oauthAuthorization")
    }

    

    /**
     * Runs get request
     * @param {string} route 
     * @param {function} callback 
     */
    get(route,callback)
    {
        let requestheaders = this.getHeader()
        // console.log('headers',this.getHeader())
        fetch(`${this.api_url}${this.api_prefix}${route}`, {
            method: 'GET',
            headers: requestheaders
        })
            .then(response => response.json())
            .then(json => callback(json))
            .catch(error => console.error('Error:', error))
    }
 
    /**
     * Runs POST request
     * @param {string} route 
     * @param {object} data 
     * @param {function} callback 
     */
    post(route,data,callback)
    {
        let requestheaders = this.getHeader()
        
        fetch(`${this.api_url}${this.api_prefix}${route}`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: requestheaders
        })
            .then(response => response.json())
            .then(json => callback(json))
            .catch(error => console.error('Error:', error))

    }

    /**
     * Runs PUT request
     * @param {string} route 
     * @param {object} data 
     * @param {function} callback 
     */
    put(route,data,callback)
    {
        let requestheaders = this.getHeader()
        // console.log(`requestheaders`,requestheaders)
        fetch(`${this.api_url}${this.api_prefix}${route}`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: requestheaders
        })
            .then(response => response.json())
            .then(json => callback(json))
            .catch(error => console.error('Error:', error))

    }
    
    /**
     * Runs DELETE request
     * @param {string} route 
     * @param {function} callback 
     */
    delete(route,callback)
    {
        let requestheaders = this.getHeader() 
        
        fetch(`${this.api_url}${this.api_prefix}${route}`, {
            method: 'DELETE',
            headers: requestheaders
        })
            .then(response => response.json())
            .then(json => callback(json))
            .catch(error => console.error('Error:', error))

    }

    /**
     * Handles upload of only 1 file
     * IMPORTANT: Do not use Content-Type in header. According to [this reference](https://github.com/matthew-andrews/isomorphic-fetch/issues/100)
     *            'The browser should add it automatically (including the boundary).' This way works fine. 
     * @see https://www.thepolyglotdeveloper.com/2017/12/upload-files-remote-web-service-vuejs-web-application/
     * @param {*} route 
     * @param {*} data 
     * @param {*} callback 
     */
    upload(route,data,callback)
    {   
        const requestheaders = {
            "Authorization": `Bearer ${this.getToken()}`,
            "Accept": "application/json",
        } 
        // console.log('upload',requestheaders)
        let formData = new FormData();

        // console.log('upload',data.id)
        formData.append('file', data.file,data.file.name);
        formData.append('id', data.id); 

        // console.log('formdata',formData)

        fetch(`${this.api_url}${this.api_prefix}${route}`, {
            method: 'POST',
            body: formData,
            headers: requestheaders
        })
            .then(response => response.json())
            .then(json => callback(json))
            .catch(error => console.error('Error:', error))

    } 

    /**
     * Gets authorization for a given oauth route
     * 
     * @param {string} username 
     * @param {string} password 
     * @param {function} callback 
     */
    getAuthorization(username,password,callback)
    {

        this.setCredentials(username,password)
        let credentials = this.getCredentials()
        let route = ApiCredential.oauth.authorization_route
        // console.log(`${this.api_url}${route}`,credentials)
        fetch(`${this.api_url}${route}`, {
            method: 'POST',
            body: JSON.stringify(credentials),
            headers:{
                'Content-type': "application/json",
                "Accept": "application/json"
            }
        })
            .then(response => response.json())
            .then((json) => {
                // console.log('service')
                this.setHeader(json.access_token)
                this.setToken(json.access_token)
                callback(json)
            })
            .catch(error => console.error('Error:', error))
    }

}

export default new HttpService